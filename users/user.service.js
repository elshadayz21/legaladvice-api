const pool = require ("../config/database" )

const Create = (data, callback) => {
        pool.query('INSERT INTO  users (first_name,last_name, branch_or_district_or_HQ, process,sub_process,role,is_approved,email,password) values(?,?,?,?,?,?,?,?,?)',
        [
            data.first_name,
            data.last_name, 
            data.branch_or_district_or_HQ, 
            data.process,
            data.sub_process,
            data.role,
            data.is_approved,
            data.email,
            data.password
        ],
        (error,results,fields) => {
            if(error) {
                return callback(error)
            }
            return callback(null,results)
        } )

    }

const Get = (callback) =>{{
  pool.query(`SELECT * FROM users`, [], (errors, results, fields) => {
    if (errors) {
      return callback(errors);
    }
    return callback(null, results);
  });
}}

const GetById = (user_id, callback) =>{
  pool.query(
    `SELECT * FROM users WHERE user_id=?`,
    [user_id],
    (errors, results, fields) => {
      if (errors) {
        return callback(errors);
      }
      return callback(null, results[0]);
    }
  );
}
function GetByEmail(email, callback) {
  pool.query(
    `SELECT * FROM users WHERE email=?`,
    [email],
    (errors, results, fields) => {
      if (errors) {
        return callback(errors);
      }
      return callback(null, results[0] ? results[0] : results) ;
    }
  );
}


const Update = (data, callback) =>{ {
  pool.query(
    `UPDATE users SET first_name=?,last_name=?, branch_or_district_or_HQ=?, process=?,sub_process=?,role=?,is_approved=?,email=?,password=? WHERE user_id=?`,
    [
          data.first_name,
            data.last_name, 
            data.branch_or_district_or_HQ, 
            data.process,
            data.sub_process,
            data.role,
            data.is_approved,
            data.email,
            data.password,
            data.user_id
    ],
    (errors, results, fields) => {
      if (errors) {
        return callback(errors);
      }
      return callback(null, results);
    }
  );
}} 

const Delete = (user_id, callback) =>{ {
  pool.query(
    `Delete from users WHERE user_id=?`,
    [user_id],
    (errors, results, fields) => {
      if (errors) {
        return callback(errors);
      }
      return callback(null, results[0]? results[0] : results);
    }
  );
 }}
// export function GetByMobile(mobile, callback) {
//   mysql.query(
//     `SELECT * FROM customer WHERE mobile=?`,
//     [mobile],
//     (errors, results, fields) => {
//       if (errors) {
//         return callback(errors);
//       }
//       return callback(null, results[0]);
//     }
//   );
// }

//repeated get request

// export function Get(callback) {
//   pool.query(`SELECT * FROM users`, [], (errors, results, fields) => {
//     if (errors) {
//       return callback(errors);
//     }
//     return callback(null, results);
//   });
// }

//repeated getBYID request

// export function GetById(id, callback) {
//   mysql.query(
//     `SELECT * FROM customer WHERE id=?`,
//     [id],
//     (errors, results, fields) => {
//       if (errors) {
//         return callback(errors);
//       }
//       return callback(null, results[0]);
//     }
//   );
// }


const checkValidity = (value, callback) =>{ {
  pool.query(
    `SELECT * FROM users WHERE email=? `,
    [value],
    (errors, results, fields) => {
      if (errors) {
        return callback(errors);
      }
      return callback(null, results[0]);
    }
  );
}}
module.exports = {checkValidity,Delete,Update,GetByEmail,GetById,Get,Create}