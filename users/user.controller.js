const {
  Create,
  Get,
  GetById,
  Update,
  Delete,
  GetByEmail,
 } = require ("./user.service.js");

const bcrypt = require("bcrypt")
const{genSaltSync, hashSync} = require("bcrypt")

const {sign} = require("jsonwebtoken")
const jt = require("jsonwebtoken")
// import jwt from "jsonwebtoken";


const GetAll = (req,res)=>{
    Get( (error, results) => {
            if (error) {
              console.log(error);
              return res.status(500).json({
                success: 0,
                message: "Database Connection Error!",
              });
            }
           return res.json({
              success: 1,
              data: results,
            });
          });
}

const createUsers = (req, res, next) => {
  const body = req.body;
    const salt = bcrypt.genSaltSync(10);
    if (body.password) {
            body.password = bcrypt.hashSync(body.password, salt);
    }

    GetByEmail(body.email,(error,results)=>{
        if(!results){
             Create(body, (error, results) => {
                 if (error) {
                    console.log(error);
                    res.status(500).json({
                        success: 0,
                        message: "database connection error!",
                    });
                  }
                    res.status(200).json({
                    success: 1,
                    data: results,
                    });
        
                })
        }else{
            res.status(409).json({
                code:404,
                message:"duplicate user with same email",
                status:"failed",
            })
        }
    })
 }

 const getUserByEmail  = (req,res)=>{

    GetByEmail(req.body.email, (error, results) => {
    if (error) {
      console.log(error);
      res.status(500).json({
        success: 0,
        message: "failed to fetch user",
        status: "failed",
      });
    }
    if (results.length==0) {
      return res.status(201).json({
        success: 0,
        message: "record is not found!",
        status: "no email files",
      });
    }
    // const { password, created_at, ...others } = results;
    res.json({
      success: 1,
      data: results,
    });
  });
 }



const getUserById = (req, res) => {
     const userId = req.params.user_id;
     GetById(userId, (error, results) => {
         
         if (error) {
      console.log(error);
      res.status(500).json({
          success: 0,
          message: "failed to fetch user",
          status: "failed",
      });
    }
    if (!results) {
      return res.json({
        success: 0,
        message: "record by id not found!",
        status: "nothing",
    });
}   else{
    return res.json({
              success: 1,
              data: results
            });
      }

    // const { password, created_at, ...others } = results;
    // res.status(200).json({
    //   success: 1,
    //   data: others,
    // });
});
}


// export function getUserByEmail(req, res) {
    //   const email= req.params.email;
    //   GetByEmail(email, (error, results) => {
        //     if (error) {
            //       console.log(error);
//       res.status(500).json({
//         success: 0,
//         message: "failed to fetch user",
//         status: "failed",
//       });
//     }
//     if (!results) {
    //       return res.status(201).json({
        //         success: 0,
        //         message: "record not found!",
        //         status: "empty",
        //       });
        //     }
        //     const { password, created_at, ...others } = results;
        //     res.status(200).json({
            //       success: 1,
            //       data: others,
            //     });
            //   });
            // }
            
            
   const  updateUsers = (req, res) => {
        const body = req.body;
        const salt = bcrypt.genSaltSync(10);
        body.password = bcrypt.hashSync(body.password, salt);
        Update(body, (error, results) => {
            if (error) {
                console.log(error);
                return res.status(500).json({
                  success: 0,
                  message: "failed to update user",
                  status: "failed",
      });
    }
    if (!results.affectedRows) 
      return res.json({
            success: 0,
            message: "Record not found",
      });
    
   return res.json({
            success: 1,
             message: "Updated successfully ",
             status: "success",
    });
    // const { password, ...others } = results;
//    return res.status(200).json({
//       success: 1,
//       message: "user updated successfully!",
//       data: results,
//     });
})
}


const deleteUsers = (req, res) => {
    const user_id = req.params.user_id
  Delete(user_id, (error, results) => {
      if (error) {
          console.log(error);
    
          res.status(500).json({
              success: 0,
        message: "failed to delete user. server error",
        status: "failed",
    });
    //results !== []
    } 
    if (!results.affectedRows) 
      return res.json({
            success: 0,
            message: "Record not found",
      });
    
        return res.json({
            success: 1,
             message: "deleted successfully ",
             status: "success",
    });

});
}


const login = (req, res) => {
        GetByEmail(req.body.email, (error, results) => {
            if (error) {
      console.log(error);
      return res.status(500).json({
        success: 0,
        message: "mysql error please check your query",
        status: "failed",
      });
    }
    if (!results) {
      return res.status(201).json({
          success: 0,
          message: "no data associated with this Email!",
      });
    }
    
    const result = bcrypt.compareSync(req.body.password, results.password);
    // console.log(results[0].user_password);
    if (result) {
        results.password = undefined;
        const jsontoken = sign({ result: results }, process.env.JWT_KEY, {
            expiresIn: "1d",
        });
        return res.json({
            success: 1,
            message: "User login Successful!",
          token: jsontoken,
        });
    } else {
        return res.json({
            success: 0,
            data: "invalid email or password",
        });
    }
});
}

module.exports = {createUsers,getUserById,deleteUsers,updateUsers,GetAll,login ,getUserByEmail};