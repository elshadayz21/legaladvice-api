const { getUserById, GetAll, updateUsers, deleteUsers, login, getUserByEmail } = require ("./user.controller")

const express = require("express");
const {createUsers} = require("./user.controller");
// import { checkToken } from "../../auth/token_validation.js";
const router = express.Router();

const { checkToken } = require("../auth/token_validation")

router.get('/', GetAll)
router.post("/", checkToken, createUsers);

router.post('/email',  getUserByEmail)
router.patch("/", checkToken,  updateUsers);
router.delete("/:user_id", checkToken,  deleteUsers);
router.get("/:user_id",  getUserById);
router.post("/login", login);


module.exports=router;
