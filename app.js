const express = require("express")
const pool = require("./config/database")
require("dotenv").config();
const userRouter= require("./users/user.router.js")
const app = express()

app.use(express.json())
app.use('/api/users',userRouter)
app.listen(process.env.APP_PORT, () =>{
    console.log("server up and running on " + process.env.APP_PORT);
})

module.exports = app